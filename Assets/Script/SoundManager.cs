﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class SoundManager : MonoBehaviour {

    [System.Serializable]
    public class Sound{
        public string _text;
        public AudioClip _audio;
        public string GetText() { return _text; }
        public AudioClip GetAudio() { return _audio; }
        public float GetDuration() { return _audio.length; }
    }

    public Sound[] _trainingSound;
    public Sound[] _bossSound;
    // Use this for initialization
    public enum SoundType { Training, Boss}
    public Sound GetRandomSound(SoundType type)
    {
        if (type == SoundType.Training)
            return _trainingSound[Random.Range(0, _trainingSound.Length)];
        return _bossSound[Random.Range(0, _bossSound.Length)];
    }
    public Sound GetSound(int index, SoundType type)
    { Sound[] selectedSounds = type == SoundType.Training ? _trainingSound : _bossSound;
        if (index < 0) index= 0;
        if (index >= selectedSounds.Length) index=selectedSounds.Length-1;
        return selectedSounds[index];
    }

    public int GetBossLength() { return _bossSound.Length; }
    public int GetTrainingLength() { return _trainingSound.Length; }

}
