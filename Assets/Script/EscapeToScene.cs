using UnityEngine;
using System.Collections;

public class EscapeToScene : MonoBehaviour {


    public LoadScene loadScene;
    void Update () {

        if (loadScene != null && (Input.GetKeyUp(KeyCode.Escape) || Input.GetButtonUp("Cancel")))
        {
           
                loadScene.LoadSelectedSceneWithDelay();
                Destroy(this);
                return;

        }
	}
}
