﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SoundSwitcher : MonoBehaviour {
    
    public SoundManager _soundManager;
    public Image _background;
    public Image _forground;
    public Text _text;
    public AudioSource _audioSource;

    public float _delayBeforeWord=0.5f;
    public float _delayAfterWord = 0.5f;

    private float _timeBeforeNextInitial;
    private float _timeBeforeNext;

    public float _timeSpeed = 0.001f;
    public float _maxSpeed = 1.6f;

    public Color[] _friendly = new Color[] { new Color(0.6f, 1f, 0.6f), new Color(0.6f, 1F, 1f), new Color(0.6f, 0.6f, 1f) } ;
    public Color _bossBackground= new Color(1,0.3f, 0.3f);
    public Color _bossForground = new Color(1, 0.6f, 0.6f);

    [Header("Debug")]
    public float _timeScale = 1f;
    
    // Use this for initialization
    IEnumerator Start () {

        int iBoss=0;
        int iCountDown = 8;
        while (true) {
            int trainingLength = _soundManager.GetTrainingLength();
            for (int i = 0; i < trainingLength ; i++)
            {
                if (Input.GetKey(KeyCode.Space))
                    iBoss = 6;
                iCountDown--;
                bool isBossSelected = iCountDown <= 0;
                
                SoundManager.Sound sound = _soundManager.GetSound(isBossSelected? iBoss : i, isBossSelected ? SoundManager.SoundType.Boss: SoundManager.SoundType.Training);
                _text.text = sound.GetText();
                _audioSource.clip = sound.GetAudio();

                if (isBossSelected)
                {
                    iCountDown = UnityEngine.Random.Range(6, 12);
                    iBoss++;
                    if (iBoss >= _soundManager.GetBossLength()) {
                        iBoss = 0;
                    }
                }

                if (!isBossSelected)
                {

                  
                    _background.color = GetTrainingColor();
                    _forground.color = 1.3f * new Color(_background.color.r, _background.color.g, _background.color.b);
                }
                else {
                    _background.color = _bossBackground;
                    _forground.color = _bossForground;

                }

                _timeBeforeNextInitial = _timeBeforeNext = _delayBeforeWord + _delayAfterWord + sound.GetDuration();
                yield return new WaitForSeconds(_delayBeforeWord);
                _audioSource.Play();

                yield return new WaitForSeconds(sound.GetDuration());
                yield return new WaitForSeconds(_delayAfterWord);
                _audioSource.Stop();
            }


        }

    }

    private Color GetTrainingColor()
    {
        return _friendly[UnityEngine.Random.Range(0, _friendly.Length)];
    }

    void Update () {
        _timeBeforeNext -= Time.deltaTime;
        _forground.fillAmount = _timeBeforeNext /_timeBeforeNextInitial;

        if (Time.timeScale < _maxSpeed) {
            Time.timeScale += _timeSpeed * Time.deltaTime;
            _timeScale = Time.timeScale;
            _audioSource.pitch= _timeScale;

        }

    }
}
